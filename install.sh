# dune-common
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git reset --hard 76cf27a3db89f21b47cbbaa2425dd40b766ceb01
cd ..

# dune-geometry
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git reset --hard 5e6c08ecc4ddcbd9e855f2a093e25c63f275d103
cd ..

# dune-grid
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git reset --hard e6255ddf4bf6a30b51b36fa28170eb5d1b798799
cd ..

# dune-localfunctions
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git reset --hard 6e57616d38482ae2923a4bcc36f8d2bc5e0a9b76
cd ..

# dune-istl
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git reset --hard 573e9e01b2ed8679875eccc0dc9f72f5dc629938
cd ..


# dune-subgrid
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
cd dune-subgrid
git reset --hard acbb64eb607421639c2405b7ce31946eff19f201
cd ..

# dumux
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git reset --hard 1901ec294f9c13e8e1729c441330bf3bc984dbc6
cd ..
