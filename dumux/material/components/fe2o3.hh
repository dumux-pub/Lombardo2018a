// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*
 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:

 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief Material properties of pure Zn \f$CaO\f$.
 */
#ifndef DUMUX_FE2O3_HH
#define DUMUX_FE2O3_HH

#include <dumux/common/exceptions.hh>

#include <cmath>
#include <iostream>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh> //assumption: hematite is dissolved

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the hematite properties
 */
template <class Scalar>
class Fe2O3
: public Components::Base<Scalar, Fe2O3<Scalar> >
, public Components::Liquid<Scalar, Fe2O3<Scalar> >
//Assumption: Fe2O3 is dissolved in water; in reality it is a dispersion, but to simplify it consider it as a solution
{
public:
    /*!
     * \brief A human readable name for the Pb.
     */
    static const char *name()
    {
        return "Fe2O3";
    }

    /*!
     * \brief The molar mass of hematite in \f$\mathrm{[kg/mol]}\f$.
     */
    static Scalar molarMass()
    {
        return 159.7e-3;
    }
    /*!
     * \brief The diffusion Coefficient \f$\mathrm{[m^2/s]}\f$ of Pb in water.
     * \param temperature absolute temperature in \f$\mathrm{[K]}\f$
     * \param pressure of the phase in \f$\mathrm{[Pa]}\f$
     */
    /*!
     * \brief The mass density \f$\mathrm{[kg/m^3]}\f$ of Pb.
     */
    static Scalar liquidDensity(Scalar temperature)
    {
        return 5200;//Packman 2005
    }

//     /*!
//      * \brief The specific heat capacity \f$\mathrm{[J/kg K]}\f$ of Pb.
//      */
//     static Scalar liquidHeatCapacity(Scalar temperature)
//     {
//         return,
//     }
//
//     /*!
//      * \brief The thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the porous material.
//      */
//     static Scalar liquidThermalConductivity(Scalar temperature)
//     {
//         return ;  //Nagel et al. (2014)
//     }
    };

} // end namespace Components

} // end namespace Dumux
#endif
