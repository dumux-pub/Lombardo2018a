// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:

 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief Material properties of pure Zn \f$CaO\f$.
 */
#ifndef DUMUX_PO4_3_HH
#define DUMUX_PO4_3_HH


#include <dumux/common/exceptions.hh>

#include <cmath>
#include <iostream>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the Pb properties
 */
template <class Scalar>
class PO4_3
: public Components::Base<Scalar, PO4_3<Scalar> >
, public Components::Liquid<Scalar, PO4_3<Scalar> >
// Pb is dissolved in water
{
public:
    /*!
     * \brief A human readable name for the PO4_3.
     */
    static const char *name()
    {
        return "PO4_3";
    }

    /*!
     * \brief The molar mass of PO4_3 in \f$\mathrm{[kg/mol]}\f$.
     */
    static Scalar molarMass()
    {
        return 94.97e-3;
    }
    /*!
     * \brief The diffusion Coefficient \f$\mathrm{[m^2/s]}\f$ of Pb in water.
     * \param temperature absolute temperature in \f$\mathrm{[K]}\f$
     * \param pressure of the phase in \f$\mathrm{[Pa]}\f$
     */
  /*  static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
      return 0.824e-9; //Quelle (Vanysek1992)
    } */
    /*!
     * \brief The mass density \f$\mathrm{[kg/m^3]}\f$ of Pb.
     */
    static Scalar liquidDensity(Scalar temperature)
    {
        return 1870;
    }

    /*!
     * \brief The specific heat capacity \f$\mathrm{[J/kg K]}\f$ of PO4_3.
    */
};

} // end namespace Components

} // end namespace Dumux

#endif
