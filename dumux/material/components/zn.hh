// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:

 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief Material properties of pure Zn \f$CaO\f$.
 */
#ifndef DUMUX_ZN_HH
#define DUMUX_ZN_HH

#include <dumux/common/exceptions.hh>

#include <cmath>
#include <iostream>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>
//#include <dumux/material/components/solid.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the Zn properties
 */
template <class Scalar>
class Zn
: public Components::Base<Scalar, Zn<Scalar> >
, public Components::Liquid<Scalar, Zn<Scalar> >
// Zn is dissolved in water
{
public:
    /*!
     * \brief A human readable name for the Zn.
     */
    static const char *name()
    {
        return "Zn";
    }
    /*!
     * \brief The molar mass of Zn in \f$\mathrm{[kg/mol]}\f$.
     */
    static Scalar molarMass()
    {
        return 65.39e-3;
    }

    /*!
     * \brief The diffusion Coefficient \f$\mathrm{[m^2/s]}\f$ of Zn in water.
     * \param temperature absolute temperature in \f$\mathrm{[K]}\f$
     * \param pressure of the phase in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
        return 0.703e-9; //Quelle (Vanysek1992)
    }
    /*!
     * \brief The mass density \f$\mathrm{[kg/m^3]}\f$ of Zn.
     */
    static Scalar liquidDensity(Scalar temperature)
    {
        return 7133;
    }

    /*!
     * \brief The specific heat capacity \f$\mathrm{[J/kg K]}\f$ of Zn.
     */
    static Scalar liquidHeatCapacity(Scalar temperature)
    {
        return 388;  //Binder1999
    }

    /*!
     * \brief The thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the porous material.
     */
    static Scalar liquidThermalConductivity(Scalar temperature)
    {
        return  116;  //Nagel et al. (2014)
    }
};

} // end namespace Components

} // end namespace Dumux

#endif
