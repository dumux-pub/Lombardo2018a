// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:

 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Components
 * \brief Material properties of pure Calcium-Oxide \f$CaO\f$.
 */
#ifndef DUMUX_Cu_HH
#define DUMUX_Cu_HH

#include <dumux/common/exceptions.hh>

#include <cmath>
#include <iostream>

#include <dumux/material/components/base.hh>//TODO statt base?
#include <dumux/material/components/liquid.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the Cu properties
 */
template <class Scalar>
class Cu
: public Components::Base<Scalar, Cu<Scalar> >
, public Components::Liquid<Scalar, Cu<Scalar> >
// Cu is dissolved in water
{
public:
    /*!
     * \brief A human readable name for the Cu.
     */
    static const char *name()
    {
        return "Cu";
    }

    /*!
     * \brief The molar mass of Cu in \f$\mathrm{[kg/mol]}\f$.
     */
    static Scalar molarMass()
    {
        return 65.546e-3;
    }

    /*!
     * \brief The diffusion Coefficient \f$\mathrm{[m^2/s]}\f$ of Cu in water.
     * \param temperature absolute temperature in \f$\mathrm{[K]}\f$
     * \param pressure of the phase in \f$\mathrm{[Pa]}\f$
     */
  /*  static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
       return 0.714e-9; //Quelle (Vanysek1992)
    } */
    /*!
     * \brief The mass density \f$\mathrm{[kg/m^3]}\f$ of Cu.
     */
    static Scalar liquidDensity(Scalar temperature)
    {
        return 8954;
    }

    /*!
     * \brief The specific heat capacity \f$\mathrm{[J/kg K]}\f$ of Cu.
     */
    static Scalar liquidHeatCapacity(Scalar temperature)
    {
        return 385;  //Nagel et al. (2014) : 934 J/kgK //TODO nochmal Namen überprüfen
    }

    /*!
     * \brief The thermal conductivity \f$\mathrm{[W/(m K)]}\f$ of the porous material.
     */
    static Scalar liquidThermalConductivity(Scalar temperature)
    {
        return  401;  //Nagel et al. (2014)
    }
};

} // end namespace Components

} // end namespace Dumux

#endif
