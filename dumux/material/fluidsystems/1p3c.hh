// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidsystems
 * \brief @copybrief Dumux::FluidSystems::LiquidPhaseWithFe2O3
 */
#ifndef DUMUX_LIQUID_PHASE_HH
#define DUMUX_LIQUID_PHASE_HH

#include <cmath>
#include <cassert>
#include <limits>

#include <dune/common/exceptions.hh>
//Water component
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/components/h2o.hh>

#include <dumux/material/components/fe2o3.hh>
#include <dumux/material/components/zn.hh>
#include <dumux/material/components/po43.hh>
#include <dumux/material/components/cu.hh>



#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/components/componenttraits.hh>

namespace Dumux {
namespace FluidSystems {

/*!
 * \ingroup Fluidsystems
 * \brief A liquid phase consisting of a single component
 */
template <class Scalar>
class LiquidPhaseWithFe2O3
: public Base<Scalar, LiquidPhaseWithFe2O3<Scalar> >
{
    using ThisType = LiquidPhaseWithFe2O3<Scalar>;
    using Base = Dumux::FluidSystems::Base<Scalar, ThisType>;

    using TabulatedH2O = Components::TabulatedComponent<Dumux::Components::H2O<Scalar> >;
    using SimpleFe2O3 = Dumux::Components::Fe2O3<Scalar>;
    using SimpleZn = Dumux::Components::Zn<Scalar>;
    using SimpleCu = Dumux::Components::Cu<Scalar>;
    using SimplePO4_3 = Dumux::Components::PO4_3<Scalar>;
public:
    using ParameterCache = NullParameterCache;

    using H2O= TabulatedH2O; //components of pure water
    using Fe2O3 = SimpleFe2O3; //!< The components for pure hematite
    using Zn = SimpleZn; //!< The components for pure zink
    using Cu = SimpleCu; //!< The components for pure copper
    using PO4_3 = SimplePO4_3; //!< The components for pure phosphate

    static constexpr int numPhases = 1;  //!< Number of phases in the fluid system
    static constexpr int phase0Idx = 0; //!< index of the only phase

    static constexpr int liquidPhaseIdx = phase0Idx;
    static constexpr int numComponents =5; //!< Number of components in the fluid system

    static constexpr int H2OIdx = 0; //!< index of the component water
    static constexpr int Fe2O3Idx = 1; //index of hematite
    static constexpr int ZnIdx = 2; //index of Zn
    static constexpr int CuIdx = 3; //index of Cu
    static constexpr int PO4_3Idx = 4; //index of PO4_3

    static constexpr int comp0Idx = H2OIdx; //index of the first component
    static constexpr int comp1Idx = Fe2O3Idx;
    static constexpr int comp2Idx = ZnIdx;
    static constexpr int comp3Idx = CuIdx;
    static constexpr int comp4Idx = PO4_3Idx;


    /*!
     * \brief Initialize the fluid system's static parameters generically
     *
     * If a tabulated H2O component is used, we do our best to create
     * tables that always work.
     */
    static void init()
    {
        init(/*tempMin=*/273.15,
             /*tempMax=*/623.15,
             /*numTemp=*/100,
             /*pMin=*/-10.,
             /*pMax=*/20e6,
             /*numP=*/200);
    }

    /*!
     * \brief Initialize the fluid system's static parameters using
     *        problem specific temperature and pressure ranges
     *
     * \param tempMin The minimum temperature used for tabulation of water \f$\mathrm{[K]}\f$
     * \param tempMax The maximum temperature used for tabulation of water\f$\mathrm{[K]}\f$
     * \param nTemp The number of ticks on the temperature axis of the  table of water
     * \param pressMin The minimum pressure used for tabulation of water \f$\mathrm{[Pa]}\f$
     * \param pressMax The maximum pressure used for tabulation of water \f$\mathrm{[Pa]}\f$
     * \param nPress The number of ticks on the pressure axis of the  table of water
     */
    static void init(Scalar tempMin, Scalar tempMax, unsigned nTemp,
                     Scalar pressMin, Scalar pressMax, unsigned nPress)
    {
        if (H2O::isTabulated)
        {
            H2O::init(tempMin, tempMax, nTemp,
                               pressMin, pressMax, nPress);
        }
    }

    /****************************************
     * Fluid phase related static parameters
     ****************************************
     */

    /*
     * \brief Return the human readable name of a fluid phase
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static std::string phaseName(int phaseIdx = 0)
    { return "water"; }

    /*!
     * \brief A human readable name for the component.
     *
     * \param compIdx The index of the component to consider
     */
    static std::string componentName(int compIdx)
    {
        assert(0 <=compIdx && compIdx < numComponents);

            if(compIdx == H2OIdx)
                return  H2O::name();
            else if (compIdx == Fe2O3Idx)
                return  Fe2O3::name();
            else if(compIdx == ZnIdx)
                return Zn::name();
            else if(compIdx == CuIdx)
                return Cu::name();
            else if(compIdx == PO4_3Idx)
                return PO4_3::name();
            DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
    }

    /*!
     * \brief There is only one phase, so not mass transfer between phases can occur
     */
    static constexpr bool isMiscible()
    { return false; }

    /*!
     * \brief Returns whether the fluid is a liquid
     */
    static constexpr bool isLiquid(int phaseIdx = 0)
    { return true; }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are independent on the fluid composition. This assumption is true
     * if only a single component is involved. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isIdealMixture(int phaseIdx = 0)
    { return false; }

    /*!
     * \brief Returns true if the fluid is assumed to be compressible
     */
    static constexpr bool isCompressible(int phaseIdx = 0)
    {
        return false;
    }

    /*!0
     * \brief Returns true if the fluid viscosity is constant
     */
    static constexpr bool viscosityIsConstant(int phaseIdx = 0)
    { return false; }

    /*!
     * \brief Returns true if the fluid is assumed to be an ideal gas
     */
    static constexpr bool isIdealGas(int phaseIdx = 0)
    { return false; /* we're a liquid! */ }

    /*!
     * \brief The mass in \f$\mathrm{[kg]}\f$ of one mole of the component.
     */
    static Scalar molarMass(int compIdx)
    {
            assert(0 <=compIdx && compIdx < numComponents);

            if(compIdx == H2OIdx)
                return  H2O::molarMass();
            else if (compIdx == Fe2O3Idx)
                return  Fe2O3::molarMass();
            else if (compIdx == ZnIdx)
                return Zn::molarMass();
            else if (compIdx == CuIdx)
                return Cu::molarMass();
            else if (compIdx == PO4_3Idx)
                return PO4_3::molarMass();

        DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
    }

    /*!
     * \brief The vapor pressure in \f$\mathrm{[Pa]}\f$ of the component at a given
     *        temperature.
     *
static Scalar vaporPressure(Scalar T)
    {  return Component::vaporPressure(T); }*/

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the component at a given pressure and temperature.
     */
   /* TODO static Scalar density(Scalar temperature, Scalar pressure, int phaseIdx, int compIdx)
    {

        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);git git
    } */ //TODO Do we need this?

    using Base::density;
    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the component at a given pressure and temperature.
     */
    template <class FluidState>
    static Scalar density(const FluidState &fluidState, int phaseIdx)
    {
        assert(phaseIdx == phase0Idx);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = 1e5;//fluidState.pressure(phaseIdx);
//         std::cout << "Pressure to LiquidDensity : " << p << "\n";
        const Scalar rho_Fe2O3 = Fe2O3::liquidDensity(T);
        const Scalar rho_H2O = H2O::liquidDensity(T, p);

        const Scalar x_H2O = fluidState.moleFraction(phase0Idx, H2OIdx);
        const Scalar x_Fe2O3 = fluidState.moleFraction(phase0Idx, Fe2O3Idx);

        const Scalar rho_Zn = Zn::liquidDensity(T);
        const Scalar x_Zn = fluidState.moleFraction(phase0Idx, ZnIdx);

        const Scalar rho_Cu = Cu::liquidDensity(T);
        const Scalar x_Cu = fluidState.moleFraction(phase0Idx, CuIdx);

        const Scalar rho_PO4_3 = PO4_3::liquidDensity(T);
        const Scalar x_PO4_3 = fluidState.moleFraction(phase0Idx, PO4_3Idx);

        //assumption: linear relation without "Exzessdichte" -> ideal mixture
        //moleFraction of the mixture is known: density has a "exzess2 part -> ideal mixture

        return (Fe2O3::molarMass() * x_Fe2O3 + H2O::molarMass()*x_H2O+Zn::molarMass()*x_Zn + Cu::molarMass()*x_Cu + PO4_3::molarMass()*x_PO4_3)/((Fe2O3::molarMass()*(x_Fe2O3)/rho_Fe2O3) + ((H2O::molarMass()*x_H2O)/rho_H2O)+((Zn::molarMass()*x_Zn)/rho_Zn) + ((Cu::molarMass()*x_Cu)/rho_Cu + ((PO4_3::molarMass()*x_PO4_3)/rho_PO4_3)));
        //assumption: linear relation without "Exzessdichte" -> ideal mixture
    }

    using Base::molarDensity;
    /*!
     * \brief The molar density \f$\rho_{mol,\alpha}\f$
     *   of a fluid phase \f$\alpha\f$ in \f$\mathrm{[mol/m^3]}\f$
     *
     * The molar density is defined by the
     * mass density \f$\rho_\alpha\f$ and the mean molar mass \f$\overline M_\alpha\f$:
     *
     * \f[\rho_{mol,\alpha} = \frac{\rho_\alpha}{\overline M_\alpha} \;.\f]
     */
    template <class FluidState>
    static Scalar molarDensity(const FluidState &fluidState, int phaseIdx)
    {
        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        const Scalar rho_Fe2O3 = Fe2O3::liquidDensity(T);
        const Scalar rho_H2O = H2O::liquidDensity(T, p);

        const Scalar x_H2O = fluidState.moleFraction(phase0Idx, H2OIdx);
        const Scalar x_Fe2O3 = fluidState.moleFraction(phase0Idx, Fe2O3Idx);

        const Scalar rho_Zn = Zn::liquidDensity(T);
        const Scalar x_Zn = fluidState.moleFraction(phase0Idx, ZnIdx);

        const Scalar rho_Cu = Cu::liquidDensity(T);
        const Scalar x_Cu = fluidState.moleFraction(phase0Idx, CuIdx);

        const Scalar rho_PO4_3 = PO4_3::liquidDensity(T);
        const Scalar x_PO4_3 = fluidState.moleFraction(phase0Idx, PO4_3Idx);

        return (x_Fe2O3 + x_H2O+ x_Zn + x_Cu + x_PO4_3)/((Fe2O3::molarMass()*(x_Fe2O3)/rho_Fe2O3) + ((H2O::molarMass()*x_H2O)/rho_H2O)+((Zn::molarMass()*x_Zn)/rho_Zn) + ((Cu::molarMass()*x_Cu)/rho_Cu + ((PO4_3::molarMass()*x_PO4_3)/rho_PO4_3)));
    }

    /*!
     * \brief The dynamic liquid viscosity \f$\mathrm{[N/m^3*s]}\f$ of the suspension ( Einstein-equatation )
     */
   static Scalar viscosity(Scalar temperature, Scalar pressure, int phaseIdx)
     {
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    } //TODO Do we need this?

    using Base::viscosity;
    /*!
     * \brief The dynamic liquid viscosity \f$\mathrm{[N/m^3*s]}\f$ of the pure component.
     */
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,
                            int phaseIdx)
    {
        //TODO We do not have a equatation for the influence of the dissolution on the viscosity : we only know that the dissolution makes the viscosity smaller
        assert(phaseIdx == phase0Idx);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        const Scalar rho_Fe2O3 = Fe2O3::liquidDensity(T);
        const Scalar rho_H2O = H2O::liquidDensity(T, p);

        const Scalar x_H2O = fluidState.moleFraction(phase0Idx, H2OIdx);

        const Scalar mu_H2O = H2O::liquidViscosity(T, p);

        //Einstein-equatation: We could apply this equatation for Re < 1 -> we can use it only for the flow in the porous media
        return mu_H2O * (1 + 2.5 * ( ( Fe2O3::molarMass() * (1 - x_H2O )/( rho_Fe2O3) )/( ( H2O::molarMass() * x_H2O)/( rho_H2O) + (Fe2O3::molarMass() * (1 - x_H2O))/ (rho_Fe2O3))));
    }

    using Base::binaryDiffusionCoefficient;
    /*!
      * \copybrief Base::binaryDiffusionCoefficient
      *
      * \param fluidState An arbitrary fluid state
      * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx The index of the component to consider
        * \param compJIdx The index of the component to consider
      */
     template <class FluidState>
     static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                            int phaseIdx,
                                              int compIIdx,
                                              int compJIdx)
     {
         assert(phaseIdx == phase0Idx);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);
         //Stokes-Einstein-Gleichung (Einstein1905)
         const Scalar k_b = 1.38064852e-23; //Boltzmankonstante [k_b] = J/K
         const Scalar R_ = 50e-9; // unit [R_] = m
         //oder 150e-9 Quelle Packman et al 2005
         const Scalar mu_H2O = H2O::liquidViscosity(T, p); //viscosity of water
        const Scalar D_Fe2O3_H2O = (k_b * T)/(6* M_PI * mu_H2O * R_);

         const Scalar D_H2O_Zn = 0.703e-9; //Quelle (Vanysek1992) //Diffusion coefficient from Zn in water // unit is m^2/s
         const Scalar D_H2O_Cu = 0.714e-9; //Quelle (Vanysek1992) //Diffusion coefficient from Cu in water // unit is m^2/s
         const Scalar D_H2O_PO4_3 = 0.824e-9;
         const Scalar D_Fe2O3_Zn = 0.703e-12;
         const Scalar D_Fe2O3_Cu = 0.714e-12;
        const Scalar D_Fe2O3_PO4_3 = 0.824e-12;

//         std::cout << "Phase  : " << phaseIdx << " compentent 1 : "<< compIIdx << "component 2: "<< compJIdx << " "<<std::endl;

//         if(compIIdx == compJIdx)
//             return 0;

        if (compIIdx > compJIdx)
        {
            using std::swap;
            swap(compIIdx, compJIdx);
        }
              if ( (compIIdx == H2OIdx && compJIdx == Fe2O3Idx) || (compIIdx == Fe2O3Idx && compJIdx == H2OIdx))
                 return D_Fe2O3_H2O;
             else if ((compIIdx == H2OIdx && compJIdx == ZnIdx) || (compIIdx == ZnIdx && compJIdx == H2OIdx))
                 return D_H2O_Zn;
             else if ((compIIdx == Fe2O3Idx && compJIdx == ZnIdx) || (compIIdx == ZnIdx && compJIdx == Fe2O3Idx))
                 return D_Fe2O3_Zn;
             else if ((compIIdx == H2OIdx && compJIdx == CuIdx) || (compIIdx == CuIdx && compJIdx == H2OIdx))
                 return D_H2O_Cu;
             else if ((compIIdx == Fe2O3Idx && compJIdx == CuIdx) || (compIIdx == CuIdx && compJIdx == Fe2O3Idx))
                 return D_Fe2O3_Cu;
             else if ((compIIdx == H2OIdx && compJIdx == PO4_3Idx) || (compIIdx == PO4_3Idx && compJIdx == H2OIdx))
                return D_H2O_PO4_3;
             else if ((compIIdx == Fe2O3Idx && compJIdx == PO4_3Idx) || (compIIdx == Fe2O3Idx && compJIdx == PO4_3Idx))
                 return D_Fe2O3_PO4_3;
             else if ((compIIdx == ZnIdx && compJIdx == PO4_3Idx) || (compIIdx == PO4_3Idx && compJIdx == ZnIdx))
                 return 0;
             else if ((compIIdx == CuIdx && compJIdx == PO4_3Idx) || (compIIdx == PO4_3Idx && compJIdx == CuIdx))
                 return 0;
             else if ((compIIdx == ZnIdx && compJIdx == CuIdx) || (compIIdx == CuIdx && compJIdx == ZnIdx))
                 return 0;

             else
                DUNE_THROW(Dune::InvalidStateException, "Invalid component combination for diffusion coefficient " << phaseIdx);

            // std::cout << "Phase  "<<std::endl;
   }

//     using Base::ternaryDiffusionCoefficient;

    //! Ternary diffusion coefficient
/*  template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                             int phaseIdx,
                                             int compIIdx,
                                             int compJIdx)
    {
        if(compJIdx != H2OIdx)
            return 0.0;
//         if (compIIdx > compJIdx)
//         {
//             using std::swap;
//             swap(compIIdx, compJIdx);
//         }

//         assert(compJIdx == H2OIdx);
        assert(compJIdx != compIIdx);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);


        //Stokes-Einstein-Gleichung (Einstein1905)
        const Scalar k_b = 1.38064852e-23; //Boltzmankonstante [k_b] = J/K
        const Scalar R_ = 50.0e-9; // unit [R_] = m
        //oder 150e-9 Quelle Packman et al 2005
        const Scalar mu_H2O = H2O::liquidViscosity(T, p); //viscosity of water



        assert(phaseIdx == 0);
        assert(0 <=compIIdx && compIIdx < numComponents);

        const Scalar x_H2O = fluidState.moleFraction(phase0Idx, H2OIdx);
        const Scalar x_Fe2O3 = fluidState.moleFraction(phase0Idx, Fe2O3Idx);

        const Scalar x_Zn = fluidState.moleFraction(phase0Idx, ZnIdx);
        const Scalar x_Cu = fluidState.moleFraction(phase0Idx, CuIdx);
        const Scalar x_PO4_3 = fluidState.moleFraction(phase0Idx, PO4_3Idx);

        const Scalar D_Fe2O3_H2O = (k_b * T)/(6* M_PI * mu_H2O * R_);

        const Scalar D_H2O_Zn = 0.703e-9;
        const Scalar D_Zn_Fe2O3 = D_H2O_Zn *1.0e-3;//assmumption (has to get improved)

        const Scalar D_H2O_Cu = 0.714e-9;
        const Scalar D_Cu_Fe2O3 = D_H2O_Cu *1.0e-3; //assmumption (has to get improved)

        const Scalar D_H2O_PO4_3 = 0.824e-9;
        const Scalar D_PO4_3_Fe2O3 = D_H2O_Cu * 1.0e-3; //assmumption (has to get improved)


        if (compIIdx == H2OIdx)
        {
            return (1-x_H2O)/( (1-x_H2O-x_Zn)/(D_Fe2O3_H2O) + x_Zn/D_H2O_Zn + x_Cu/D_H2O_Cu + x_PO4_3/D_H2O_PO4_3); //Quelle (Vanysek1992) //Diffusion coefficient from Zn in water // unit is m^2/s
        }
        else if (compIIdx == Fe2O3Idx)
        {
            return (x_Zn + x_H2O)/( x_Zn/D_Zn_Fe2O3 + x_H2O/D_Fe2O3_H2O + x_Cu/D_Cu_Fe2O3 + x_PO4_3/D_PO4_3_Fe2O3);
        }
        else if(compIIdx == ZnIdx)
        {
            return (1-x_Zn)/( x_H2O/D_H2O_Zn + (x_Fe2O3)/(D_Zn_Fe2O3));
        }
        else if(compIIdx == CuIdx)
        {
            return (1-x_Cu)/( x_H2O/D_H2O_Cu + (x_Fe2O3)/(D_Cu_Fe2O3));
        }
        else
        {
            return (1-x_PO4_3)/( x_H2O/D_H2O_PO4_3 + (x_Fe2O3)/(D_PO4_3_Fe2O3));

        }
        }*/

};

} // namespace FluidSystems
} // namespace

#endif
