// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidsystems
 * \brief @copybrief Dumux::FluidSystems::LiquidPhaseWithFe2O3
 */
#ifndef DUMUX_LIQUID_PHASE_HH
#define DUMUX_LIQUID_PHASE_HH

#include <cmath>
#include <cassert>
#include <limits>

#include <dune/common/exceptions.hh>
//Water component
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/components/h2o.hh>

#include <dumux/material/components/zn.hh>
#include <dumux/material/components/po43.hh>
#include <dumux/material/components/cu.hh>



#include <dumux/material/fluidsystems/base.hh>
#include <dumux/material/components/componenttraits.hh>

namespace Dumux {
namespace FluidSystems {

/*!
 * \ingroup Fluidsystems
 * \brief A liquid phase consisting of a single component
 */
template <class Scalar>
class LiquidPhaseWithFe2O3
: public Base<Scalar, LiquidPhaseWithFe2O3<Scalar> >
{
    using ThisType = LiquidPhaseWithFe2O3<Scalar>;
    using Base = Dumux::FluidSystems::Base<Scalar, ThisType>;

    using TabulatedH2O = Components::TabulatedComponent<Dumux::Components::H2O<Scalar> >;
    using SimpleZn = Dumux::Components::Zn<Scalar>;
    using SimpleCu = Dumux::Components::Cu<Scalar>;
    using SimplePO4_3 = Dumux::Components::PO4_3<Scalar>;
public:
    using ParameterCache = NullParameterCache;

    using H2O= TabulatedH2O; //components of pure water
    using Zn = SimpleZn; //!< The components for pure zink
    using Cu = SimpleCu; //!< The components for pure copper

    static constexpr int numPhases = 1;  //!< Number of phases in the fluid system
    static constexpr int phase0Idx = 0; //!< index of the only phase

    static constexpr int liquidPhaseIdx = phase0Idx;
    static constexpr int numComponents =3; //!< Number of components in the fluid system

    static constexpr int H2OIdx = 0; //!< index of the component water
    static constexpr int ZnIdx = 1; //index of Zn
    static constexpr int CuIdx = 2; //index of Cu


    static constexpr int comp0Idx = H2OIdx; //index of the first component
    static constexpr int comp1Idx = ZnIdx;
    static constexpr int comp2Idx = CuIdx;


    /*!
     * \brief Initialize the fluid system's static parameters generically
     *
     * If a tabulated H2O component is used, we do our best to create
     * tables that always work.
     */
    static void init()
    {
        init(/*tempMin=*/273.15,
             /*tempMax=*/623.15,
             /*numTemp=*/100,
             /*pMin=*/-10.,
             /*pMax=*/20e6,
             /*numP=*/200);
    }

    /*!
     * \brief Initialize the fluid system's static parameters using
     *        problem specific temperature and pressure ranges
     *
     * \param tempMin The minimum temperature used for tabulation of water \f$\mathrm{[K]}\f$
     * \param tempMax The maximum temperature used for tabulation of water\f$\mathrm{[K]}\f$
     * \param nTemp The number of ticks on the temperature axis of the  table of water
     * \param pressMin The minimum pressure used for tabulation of water \f$\mathrm{[Pa]}\f$
     * \param pressMax The maximum pressure used for tabulation of water \f$\mathrm{[Pa]}\f$
     * \param nPress The number of ticks on the pressure axis of the  table of water
     */
    static void init(Scalar tempMin, Scalar tempMax, unsigned nTemp,
                     Scalar pressMin, Scalar pressMax, unsigned nPress)
    {
        if (H2O::isTabulated)
        {
            H2O::init(tempMin, tempMax, nTemp,
                               pressMin, pressMax, nPress);
        }
    }

    /****************************************
     * Fluid phase related static parameters
     ****************************************
     */

    /*
     * \brief Return the human readable name of a fluid phase
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static std::string phaseName(int phaseIdx = 0)
    { return "water"; }

    /*!
     * \brief A human readable name for the component.
     *
     * \param compIdx The index of the component to consider
     */
    static std::string componentName(int compIdx)
    {
        assert(0 <=compIdx && compIdx < numComponents);

            if(compIdx == H2OIdx)
                return  H2O::name();
            else if(compIdx == ZnIdx)
                return Zn::name();
            else if(compIdx == CuIdx)
                return Cu::name();
            DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
    }

    /*!
     * \brief There is only one phase, so not mass transfer between phases can occur
     */
    static constexpr bool isMiscible()
    { return false; }

    /*!
     * \brief Returns whether the fluid is a liquid
     */
    static constexpr bool isLiquid(int phaseIdx = 0)
    { return true; }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are independent on the fluid composition. This assumption is true
     * if only a single component is involved. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static constexpr bool isIdealMixture(int phaseIdx = 0)
    { return false; }

    /*!
     * \brief Returns true if the fluid is assumed to be compressible
     */
    static constexpr bool isCompressible(int phaseIdx = 0)
    {
        return false;
    }

    /*!0
     * \brief Returns true if the fluid viscosity is constant
     */
    static constexpr bool viscosityIsConstant(int phaseIdx = 0)
    { return false; }

    /*!
     * \brief Returns true if the fluid is assumed to be an ideal gas
     */
    static constexpr bool isIdealGas(int phaseIdx = 0)
    { return false; /* we're a liquid! */ }

    /*!
     * \brief The mass in \f$\mathrm{[kg]}\f$ of one mole of the component.
     */
    static Scalar molarMass(int compIdx)
    {
            assert(0 <=compIdx && compIdx < numComponents);

            if(compIdx == H2OIdx)
                return  H2O::molarMass();
            else if (compIdx == ZnIdx)
                return Zn::molarMass();
            else if (compIdx == CuIdx)
                return Cu::molarMass();

        DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
    }

    /*!
     * \brief The vapor pressure in \f$\mathrm{[Pa]}\f$ of the component at a given
     *        temperature.
     *
static Scalar vaporPressure(Scalar T)
    {  return Component::vaporPressure(T); }*/

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the component at a given pressure and temperature.
     */

    using Base::density;
    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the component at a given pressure and temperature.
     */
    template <class FluidState>
    static Scalar density(const FluidState &fluidState, int phaseIdx)
    {
       // return 1000;
        assert(phaseIdx == phase0Idx);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = 1e5;//fluidState.pressure(phaseIdx);
        const Scalar rho_H2O = H2O::liquidDensity(T, p);

        const Scalar x_H2O = fluidState.moleFraction(phase0Idx, H2OIdx);

        const Scalar rho_Zn = Zn::liquidDensity(T);
        const Scalar x_Zn = fluidState.moleFraction(phase0Idx, ZnIdx);

        const Scalar rho_Cu = Cu::liquidDensity(T);
        const Scalar x_Cu = fluidState.moleFraction(phase0Idx, CuIdx);

        //assumption: linear relation without "Exzessdichte" -> ideal mixture
        //moleFraction of the mixture is known: density has a "exzess2 part -> ideal mixture

        return ( H2O::molarMass()*x_H2O+Zn::molarMass()*x_Zn + Cu::molarMass()*x_Cu)/(((H2O::molarMass()*x_H2O)/rho_H2O)+((Zn::molarMass()*x_Zn)/rho_Zn) + (Cu::molarMass()*x_Cu)/rho_Cu);
        //assumption: linear relation without "Exzessdichte" -> ideal mixture
    }

    using Base::molarDensity;
    /*!
     * \brief The molar density \f$\rho_{mol,\alpha}\f$
     *   of a fluid phase \f$\alpha\f$ in \f$\mathrm{[mol/m^3]}\f$
     *
     * The molar density is defined by the
     * mass density \f$\rho_\alpha\f$ and the mean molar mass \f$\overline M_\alpha\f$:
     *
     * \f[\rho_{mol,\alpha} = \frac{\rho_\alpha}{\overline M_\alpha} \;.\f]
     */
    template <class FluidState>
    static Scalar molarDensity(const FluidState &fluidState, int phaseIdx)
    {
        return 5.5e4;
        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = 1e5; //fluidState.pressure(phaseIdx);

        const Scalar rho_H2O = H2O::liquidDensity(T, p);

        const Scalar x_H2O = fluidState.moleFraction(phase0Idx, H2OIdx);

        const Scalar rho_Zn = Zn::liquidDensity(T);
        const Scalar x_Zn = fluidState.moleFraction(phase0Idx, ZnIdx);

        const Scalar rho_Cu = Cu::liquidDensity(T);
        const Scalar x_Cu = fluidState.moleFraction(phase0Idx, CuIdx);

        return ( x_H2O+ x_Zn + x_Cu )/(((H2O::molarMass()*x_H2O)/rho_H2O)+((Zn::molarMass()*x_Zn)/rho_Zn) + ((Cu::molarMass()*x_Cu)/rho_Cu));
    }

    /*!
     * \brief The dynamic liquid viscosity \f$\mathrm{[N/m^3*s]}\f$ of the suspension ( Einstein-equatation )
     */
   static Scalar viscosity(Scalar temperature, Scalar pressure, int phaseIdx)
     {
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    using Base::viscosity;
    /*!
     * \brief The dynamic liquid viscosity \f$\mathrm{[N/m^3*s]}\f$ of the pure component.
     */
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,
                            int phaseIdx)
    {
        //We do not have a equatation for the influence of the dissolution on the viscosity : we only know that the dissolution makes the viscosity smaller
        assert(phaseIdx == phase0Idx);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);

        const Scalar mu_H2O = H2O::liquidViscosity(T, p);

        return mu_H2O;
    }

    using Base::binaryDiffusionCoefficient;
    /*!
      * \copybrief Base::binaryDiffusionCoefficient
      *
      * \param fluidState An arbitrary fluid state
      * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx The index of the component to consider
        * \param compJIdx The index of the component to consider
      */
     template <class FluidState>
     static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                            int phaseIdx,
                                              int compIIdx,
                                              int compJIdx)
     {
         assert(phaseIdx == phase0Idx);

        Scalar T = fluidState.temperature(phaseIdx);
        Scalar p = fluidState.pressure(phaseIdx);
         //Stokes-Einstein-Gleichung (Einstein1905)
         const Scalar k_b = 1.38064852e-23; //Boltzmankonstante [k_b] = J/K
         const Scalar R_ = 50e-9; // unit [R_] = m
         //oder 150e-9 Quelle Packman et al 2005
         const Scalar mu_H2O = H2O::liquidViscosity(T, p); //viscosity of water

         const Scalar D_H2O_Zn = 0.703e-9; //Quelle (Vanysek1992) //Diffusion coefficient from Zn in water // unit is m^2/s
         const Scalar D_H2O_Cu = 0.714e-9; //Quelle (Vanysek1992) //Diffusion coefficient from Cu in water // unit is m^2/s
         const Scalar D_H2O_PO4_3 = 0.824e-9;


        if (compIIdx > compJIdx)
        {
            using std::swap;
            swap(compIIdx, compJIdx);
        }

        if ((compIIdx == H2OIdx && compJIdx == ZnIdx) || (compIIdx == ZnIdx && compJIdx == H2OIdx))
            return D_H2O_Zn;
        else if ((compIIdx == H2OIdx && compJIdx == CuIdx) || (compIIdx == CuIdx && compJIdx == H2OIdx))
            return D_H2O_Cu;
        else if ((compIIdx == ZnIdx && compJIdx == CuIdx) || (compIIdx == CuIdx && compJIdx == ZnIdx))
            return 0;
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid component combination for diffusion coefficient " << phaseIdx);
   }


};

} // namespace FluidSystems
} // namespace

#endif
