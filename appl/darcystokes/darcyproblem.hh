// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A simple Darcy test problem (cell-centered finite volume method). -> ********Darcy with Dirichlet and Injection in the free flow
 */
#ifndef DUMUX_DARCY_SUBPROBLEM_HH
#define DUMUX_DARCY_SUBPROBLEM_HH

#include <dumux/discretization/cellcentered/tpfa/properties.hh>

#include <dumux/porousmediumflow/1pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "1pspatialparams.hh"

//In this fluidsystem you can only chose the components zinc and phosphate: if you want to make a simulation with zinc -> x_phophate must be 0 -> input file

#include <dumux/material/fluidsystems/1p3c_phosphate_or_zinc.hh>

//For a scenario with copper or zinc you have to chose this fluidsystem
// #include <dumux/material/fluidsystems/1p3c_zinc_or_copper.hh>

//scenario in the cut-off meander with 3 components: here hematie, zinc and water
// #include <dumux/material/fluidsystems/1p3c_hematiteandzn.hh>

//scenario in the cut-off meander with hematie, zinc and water
// #include <dumux/material/fluidsystems/1p3c_hematiteandzn.hh>

#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

namespace Dumux
{
template <class TypeTag>
class DarcySubProblem;

namespace Properties
{
NEW_TYPE_TAG(DarcyOnePTwoCTypeTag, INHERITS_FROM(CCTpfaModel, OnePNC));

// Set the problem property
SET_TYPE_PROP(DarcyOnePTwoCTypeTag, Problem, Dumux::DarcySubProblem<TypeTag>);

// The fluid system
SET_PROP(DarcyOnePTwoCTypeTag, FluidSystem)
{
    using type = FluidSystems::LiquidPhaseWithFe2O3<typename GET_PROP_TYPE(TypeTag, Scalar)>;
};

// Use moles
SET_BOOL_PROP(DarcyOnePTwoCTypeTag, UseMoles, true);

// Do not replace one equation with a total mass balance
SET_INT_PROP(DarcyOnePTwoCTypeTag, ReplaceCompEqIdx, 3);

//! Use a model with constant tortuosity for the effective diffusivity
SET_TYPE_PROP(DarcyOnePTwoCTypeTag, EffectiveDiffusivityModel,
              DiffusivityConstantTortuosity<typename GET_PROP_TYPE(TypeTag, Scalar)>);


// Set the spatial paramaters type
SET_TYPE_PROP(DarcyOnePTwoCTypeTag, SpatialParams, OnePSpatialParams<TypeTag>);
}

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using FluidState = typename GET_PROP_TYPE(TypeTag, FluidState);
    using ParameterCache = typename FluidSystem::ParameterCache;
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using FVElementGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry)::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);

    // copy some indices for convenience
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    enum {
        // grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        onephaseIdx = FluidSystem::phase0Idx,
        pressureIdx = Indices::pressureIdx,

    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using GravityVector = Dune::FieldVector<Scalar, dimWorld>;
    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);

public:
    DarcySubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Darcy"), eps_(1e-7), couplingManager_(couplingManager)
    {
        FluidState fluidState;
        fluidState.setPressure(0, 1e5);
        fluidState.setTemperature(temperature());

        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");

        initialMoleFraction_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialMoleFraction");
        boundary_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Boundary");
         gamma_ = getParam<Scalar>("Problem.Gamma");

         if (getParam<bool>("Problem.EnableGravity"))
        {//2-Dim Problem

            using std::sin;
            using std::cos;
            gravity_[0] =  sin(gamma_ * M_PI / 180.0) * 9.81;
            gravity_[1] = - cos(gamma_ * M_PI / 180.0) * 9.81;
        }

        file.open("Total_mass_pm.txt");

    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const // define output
    { return true; }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}
//In a river the downhill gradient is important: So we turn the GravityVector by a multiplication with a rotation matrix !
     const GravityVector& gravity() const
    { return gravity_; }
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
      * \brief Specifies which kind of boundary condition should be
      *        used for which equation on a given boundary control volume.
      *
      * \param element The element
      * \param scvf The boundary sub control volume face
      */
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolumeFace &scvf) const
    {
        //other model setup which you can chose via the input file
        BoundaryTypes values;
       if (boundary_ ==1.0)
        {
        const auto& globalPos = scvf.center();

         if (onLeftBoundary_(globalPos))
            {
                values.setDirichlet(Indices::pressureIdx);
                values.setDirichlet(Indices::conti0EqIdx + 1);
                values.setDirichlet(Indices::conti0EqIdx + 2);
            }

            else if (onRightBoundary_(globalPos))
            {
                values.setDirichlet(Indices::pressureIdx);
                values.setDirichlet(Indices::conti0EqIdx + 1);
                values.setDirichlet(Indices::conti0EqIdx + 2);
            }
             else
            {
                values.setAllNeumann();

            }
        }
        //model setup with noflow boundary conditions in the porous media
        if( boundary_ ==0.0)
                values.setAllNeumann();

        if (couplingManager().isCoupledEntity(couplingManager().darcyIdx, scvf))
            values.setAllCouplingNeumann();

        return values;
    }
 /*!
     * \brief Evaluate the boundary conditions for a Dirichlet control volume.
     *
     * \param element The element
     * \param scvf The sub control volume face
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);
        values = initialAtPos(globalPos);
     return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        if (couplingManager().isCoupledEntity(couplingManager().darcyIdx, scvf))
            values = couplingManager().couplingData().massCouplingCondition(fvGeometry, elemVolVars, scvf);

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * \param element The element for which the source term is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scv The subcontrolvolume
     */
    template<class ElementVolumeVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    { return NumEqVector(0.0);
    }

    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param element The element
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);

        FluidState fluidState;
        fluidState.setPressure(0, 1e5);
        fluidState.setTemperature(temperature());
        fluidState.setMoleFraction(0, 0, 1);

        Scalar density = FluidSystem::density(fluidState, 0);


       /* Superposition of hydrostatic pressure in the porous medium

        -> p = p_atm + rho * g * H
        -> 0.65 is the bBoxMax[1] of the whole grid and not only of the darcy subgrid */

       values[pressureIdx] = pressure_ - density * this->gravity()[dimWorld-1]* (0.65 - globalPos[1]);

      //values of the dissolved components are 0 in the porous media!

        return values;
    }


    // \}


    //! Set the coupling managerphaseIdx = FluidSystem::phase0Idx
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    //total substance is written in a file -> shows the hyporheic exchange

    template<class GridVariables, class SolutionVector>
    void calculateGlobalMass(const GridVariables& gridVars, const SolutionVector& sol)
    {
        auto fvGeomentry = localView(this->fvGridGeometry());
        auto elemVolVars = localView(gridVars.curGridVolVars());

        NumEqVector totalMass(0.0);

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            fvGeomentry.bindElement(element);
            elemVolVars.bindElement(element, fvGeomentry, sol);

            for (const auto& scv : scvs(fvGeomentry))
            {
                const auto& volVars = elemVolVars[scv];

                for (int i = 0; i < totalMass.size(); ++i)
                    totalMass[i] += volVars.molarDensity() * volVars.moleFraction(0, i) * scv.volume();
            }
        }


        file << "Total mass in porous media is " << totalMass << std::endl;

    }
private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->fvGridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_; }

    Scalar eps_;
    Scalar pressure_;
    Scalar initialMoleFraction_;
    Scalar boundary_;
    GravityVector gravity_;
    Scalar gamma_;
    std::ofstream file;

    std::shared_ptr<CouplingManager> couplingManager_;
};
} //end namespace

#endif //DUMUX_DARCY_SUBPROBLEM_HH
