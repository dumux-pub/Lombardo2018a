// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief A simple Navier-Stokes test problem for the staggered grid (Navier-)Stokes model.
 */
#ifndef DUMUX_STOKES_SUBPROBLEM_HH
#define DUMUX_STOKES_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>


//In this fluidsystem you can only chose the components zinc and phosphate: if you want to make a simulation with zinc -> x_phophate must be 0 -> input file

#include <dumux/material/fluidsystems/1p3c_phosphate_or_zinc.hh>

//For a scenario with copper or zinc you have to chose this fluidsystem
// #include <dumux/material/fluidsystems/1p3c_zinc_or_copper.hh>

//scenario in the cut-off meander with 3 components: here hematie, zinc and water
// #include <dumux/material/fluidsystems/1p3c_hematiteandzn.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>

#include <dumux/freeflow/compositional/navierstokesncmodel.hh>
#include <dumux/freeflow/navierstokes/problem.hh>

//TODO In case of Turbulence
// #include <dumux/freeflow/compositional/komegancmodel.hh>
// #include <dumux/freeflow/rans/twoeq/komega/problem.hh>

#include <dumux/freeflow/turbulenceproperties.hh>


namespace Dumux
{
template <class TypeTag>
class StokesOnePTwoCTypeTag;

namespace Properties
{
//TODO In case of Turbulence
// NEW_TYPE_TAG(StokesOnePTwoCTypeTag, INHERITS_FROM(StaggeredFreeFlowModel, KOmegaNC));

NEW_TYPE_TAG(StokesOnePTwoCTypeTag, INHERITS_FROM(StaggeredFreeFlowModel, NavierStokesNC));

// The fluid system
SET_PROP(StokesOnePTwoCTypeTag, FluidSystem)
{
    using type = FluidSystems::LiquidPhaseWithFe2O3<typename GET_PROP_TYPE(TypeTag, Scalar)>;
};

// Set the problem property
SET_TYPE_PROP(StokesOnePTwoCTypeTag, Problem, Dumux::StokesOnePTwoCTypeTag<TypeTag>);

SET_BOOL_PROP(StokesOnePTwoCTypeTag, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(StokesOnePTwoCTypeTag, EnableGridFluxVariablesCache, true);
SET_BOOL_PROP(StokesOnePTwoCTypeTag, EnableGridVolumeVariablesCache, true);

// Do consider intertia terms
SET_BOOL_PROP(StokesOnePTwoCTypeTag, EnableInertiaTerms, true);

// Use moles
SET_BOOL_PROP(StokesOnePTwoCTypeTag, UseMoles, true);

// Do not replace one equation with a total mass balance
SET_INT_PROP(StokesOnePTwoCTypeTag, ReplaceCompEqIdx, 3);
}

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the one-phase (Navier-) Stokes problem.
 *
 * Horizontal flow from left to right with a parabolic velocity profile.
 */
template <class TypeTag>
//TODO Turbulence
//class StokesOnePTwoCTypeTag : public KOmegaProblem<TypeTag>
class StokesOnePTwoCTypeTag : public NavierStokesProblem<TypeTag>
{
    //TODO In case of turbulence
    //using ParentType = KOmegaProblem<TypeTag>;
    using ParentType = NavierStokesProblem<TypeTag>;
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    enum
    {
        // grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

    };
    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using FluidState = typename GET_PROP_TYPE(TypeTag, FluidState);
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GravityVector = Dune::FieldVector<Scalar, dimWorld>;

    using CouplingManager = typename GET_PROP_TYPE(TypeTag, CouplingManager);

public:
    StokesOnePTwoCTypeTag(std::shared_ptr<const FVGridGeometry> fvGridGeometry, std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Stokes"), eps_(1e-6), injectionState_(false), gravity_(0.0),
    couplingManager_(couplingManager)
    {

    //TODO Turbulence   Dumux::TurbulenceProperties<Scalar, dimWorld, true> turbulenceProperties;
        FluidSystem::init();
        FluidState fluidState;
        fluidState.setPressure(0, 1e5);
        fluidState.setTemperature(temperature());

        /*//TODO In case of Turbulence
          Scalar kinematicViscosity = FluidSystem::viscosity(fluidState, 0) / density;
          Scalar diameter = 1.0;*/

        inletVelocity_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Velocity");
        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");

        /* //scenario with hematite -> system of 3 components
         inletMoleFraction_Fe2O3_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletMoleFraction_Fe2O3");*/

        //the fluidsystem_phosphate_zinc is included -> so x_copper has to be commented

        //inletMoleFraction_Cu_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletMoleFraction_Cu");
        inletMoleFraction_Zn_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletMoleFraction_Zn");
        inletMoleFraction_PO4_3_= getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletMoleFraction_PO4_3");


          /*TODO //In case of Turbulence
           * dissipation_ = getParamFromGroup<Scalar>(this->paramGroup(),"Problem.InletDissipationRate",
                                   turbulenceProperties.dissipationRate(inletVelocity_, diameter, kinematicViscosity));*/

          //rotation of the gravity vector -> multiplication with matrix
        gamma_ = getParam<Scalar>("Problem.Gamma");

        if (getParam<bool>("Problem.EnableGravity"))
        {//2-Dim Problem
            using std::sin;
            using std::cos;

            gravity_[0] =  sin(gamma_ * M_PI / 180.0) * 9.81;
            gravity_[1] = - cos(gamma_ * M_PI / 180.0) * 9.81;
        }

        file_.open("Total_mass_ff.txt");

        }
//we only have a 2-dim problem:

   /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteRestartFile() const
    { return false; }

   /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    //In a river the downhill gradient is important: So we turn the GravityVector by a multiplication with a rotation matrix !
     const GravityVector& gravity() const
    { return gravity_; }

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        auto source = NumEqVector(0.0);

        /* TODO start injection after the velocity field had enough time to initialize
          Only for the scenario in the cut-off meander -> depending on the fluidsystem you have to chose your components */

/*         if(globalPos[0] > 0.52 + eps_ && globalPos[0] < this->fvGridGeometry().bBoxMax()[0] + eps_ && globalPos[1] >                             0.63 && isInjectionPeriod())
        {
            source[Indices::conti0EqIdx + 1] = inletMoleFraction_Zn_;
            source[Indices::conti0EqIdx + 2] = inletMoleFraction_PO4_3_;
            //source[Indices::conti0EqIdx + 2] = inletMoleFraction_Cu_;

         }*/
    /* For the scenario in the cut-off meander with hematite

         if(globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - 0.15 && globalPos[0] > 0.1 && isInjectionPeriod()) {
         source[Indices::conti0EqIdx + 2] = inletMoleFraction_Fe2O3_;
         }*/

        return source;

    }
    // \}

   /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        const auto& globalPos = scvf.dofPosition();

        //TODO Turbulence
        //const auto e0Idx = this->fvGridGeometry().elementMapper().index(element);


        if(onLeftBoundary_(globalPos))
        {

            /*boundary condition for scenario cut-off meander
            values.setDirichlet(Indices::conti0EqIdx + 1);
            values.setDirichlet(Indices::pressureIdx);
            values.setDirichlet(Indices::conti0EqIdx + 2);*/

            //boundary condition for scenario in the middle cource of a river
            values.setDirichlet(Indices::conti0EqIdx + 1);
            values.setDirichlet(Indices::conti0EqIdx + 2);
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);


          /*TODO Turbulence: If we include the k-omega modell
           *
          values.setDirichlet(Indices::turbulentKineticEnergyIdx);
          values.setDirichlet(Indices::dissipationIdx);*/


        }
        else if(onRightBoundary_(globalPos))
        {


           /*TODO in case of turbulence
            *
            * if(!((e0Idx == this->fvGridGeometry().numCellCenterDofs() -1 )))
            {
              values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            }*/

           //boundary condition for scenario in the middle cource of a river
            values.setOutflow(Indices::conti0EqIdx + 1);
            values.setOutflow(Indices::conti0EqIdx + 2);
            values.setDirichlet(Indices::pressureIdx);


            /* //boundary condition for scenario cut-off meander
                values.setAllSymmetry();*/


                /*TODO In case of Turbulence
                 * values.setDirichletCell(Indices::dissipationIdx);
                 * values.setOutflow(Indices::turbulentKineticEnergyEqIdx);
                 * values.setOutflow(Indices::dissipationEqIdx);*/
        }
        else if(onUpperBoundary_(globalPos))
        {
            /*TODO In case of Turbulence
             * values.setDirichletCell(Indices::dissipationIdx);
             * values.setDirichlet(Indices::turbulentKineticEnergyIdx);
             * values.setDirichlet(Indices::dissipationIdx);*/

            values.setAllSymmetry();
        }

        if(couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
        {
            /*TODO In case of Turbulence
             values.setDirichletCell(Indices::dissipationIdx);*/

            values.setCouplingNeumann(Indices::conti0EqIdx);
            values.setCouplingNeumann(Indices::conti0EqIdx + 1);
            //We do not need the coupling of the component with 0 molefraction

            //If we include hematite
            //values.setCouplingNeumann(Indices::conti0EqIdx + 2);

            values.setCouplingNeumann(scvf.directionIndex());
            values.setBJS(1-scvf.directionIndex());
        }
        //TODO In case of Turbulence
       /* if((e0Idx == this->fvGridGeometry().numCellCenterDofs() -1 ) && onRightBoundary_(globalPos))
            values.setDirichlet(Indices::conti0EqIdx);*/

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Dirichlet control volume.
     *
     * \param element The element
     * \param scvf The sub control volume face
     */


    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);
        values = initialAtPos(globalPos);

         //only for scenario in the middle cource of a river
        // start injecting after the velocity field had enough time to initialize
        if(globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_ && isInjectionPeriod()){

             values[Indices::conti0EqIdx + 1] = inletMoleFraction_Zn_;
             values[Indices::conti0EqIdx + 2] = inletMoleFraction_PO4_3_;
//              values[Indices::conti0EqIdx + 2] = inletMoleFraction_Cu_;
        }


        return values;
    }

// TODO turbulence  PrimaryVariables dirichlet(const Element &element, const SubControlVolume &scv) const
//     {
//         const auto globalPos = scv.center();
//         PrimaryVariables values(0.0);
//         values = initialAtPos(globalPos);
//
//          // start injecting after the velocity field had enough time to initialize
//         if(globalPos[0] > 0.1*this->fvGridGeometry().bBoxMax()[0] && globalPos[0] < 0.2*this->fvGridGeometry().bBoxMax()[0] + eps_ && globalPos[1] > 0.9*this->fvGridGeometry().bBoxMin()[1] && isInjectionPeriod())
//         {
//             values[Indices::conti0EqIdx + 1] = inletMoleFraction_Fe2O3_;
//             values[Indices::conti0EqIdx + 2] = inletMoleFraction_Zn_;
//             values[Indices::conti0EqIdx + 3] = inletMoleFraction_Cu_;
//             values[Indices::conti0EqIdx + 4] = inletMoleFraction_PO4_3_;
//         }
//
//           using std::pow;
// //         unsigned int elementIdx = this->fvGridGeometry().elementMapper().index(element);
// //         const auto wallDistance = ParentType::wallDistance_[elementIdx];
// //         values[Indices::dissipationEqIdx] = 6.0 * ParentType::kinematicViscosity_[elementIdx]
// //                                             / (ParentType::betaOmega() * pow(wallDistance, 2));
//         return values;
//     }
//
//       PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
//       {
//           const auto globalPos = scvf.ipGlobal();
//           PrimaryVariables values(0.0);
//         values = initialAtPos(globalPos);
//
// //         if (isOnWall(scvf))
// //         {
// //             values[Indices::turbulentKineticEnergyIdx] = 0.0;
// //             values[Indices::dissipationIdx] = 0.0;
// //         }
//
//         return values;
//       }




    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFaceVars The element face variables
     * \param scvf The boundary sub control volume face
     */
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        if(couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
        {
            values[scvf.directionIndex()] = couplingManager().couplingData().momentumCouplingCondition(fvGeometry, elemVolVars, elemFaceVars, scvf);

            const auto tmp = couplingManager().couplingData().massCouplingCondition(fvGeometry, elemVolVars, elemFaceVars, scvf);
            values[Indices::conti0EqIdx] = tmp[0];
            values[Indices::conti0EqIdx + 1] = tmp[1];
            values[Indices::conti0EqIdx + 2] = tmp[2];
        }
        return values;
    }

    // \}

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);

        FluidState fluidState;
        fluidState.setPressure(0, 1e5);
        fluidState.setTemperature(temperature());
        fluidState.setMoleFraction(0, 0, 1);

        Scalar density = FluidSystem::density(fluidState, 0);


        /*Superposition of hydrostatic pressure in the porous medium
         p = p_atm + rho * g * H
        -> we do not need a pressure difference in x-direction because the gravity is the driving force*/

       values[Indices::pressureIdx] = pressure_ - density * this->gravity()[dimWorld-1]* (this->fvGridGeometry().bBoxMax()[1] - globalPos[1]);


        values[Indices::velocityXIdx] = inletVelocity_ * (globalPos[1] - this->fvGridGeometry().bBoxMin()[1])
                                              * (2 * this->fvGridGeometry().bBoxMax()[1] - globalPos[1])
                                              / (0.25 * (this->fvGridGeometry().bBoxMax()[1] - this->fvGridGeometry().bBoxMin()[1])
                                              * (this->fvGridGeometry().bBoxMax()[1] - this->fvGridGeometry().bBoxMin()[1]));
/* TODO In case of Turbulence
         values[Indices::turbulentKineticEnergyIdx] = turbulentKineticEnergy_;
         values[Indices::dissipationIdx] = dissipation_;*/


        return values;
    }
 /*!
     * \brief Returns the intrinsic permeability of required as input
parameter for the Beavers-Joseph-Saffman boundary condition
     */
    Scalar permeability(const SubControlVolumeFace& scvf) const
    {
        return couplingManager().problem(CouplingManager::darcyIdx).spatialParams().permeabilityAtPos(scvf.center());
    }
    /*
     * \brief Returns the intrinsic permeability of required as input parameter for the Beavers-Joseph-Saffman boundary condition

     * \brief Returns the alpha value required as input parameter for the Beavers-Joseph-Saffman boundary condition
     */
    Scalar alphaBJ(const SubControlVolumeFace& scvf) const
    {
        return 1.0;
    }

    void setInjectionState(const bool yesNo)
    {
        injectionState_ = yesNo;
    }

//For the hematite scenario

//     void setInjectionStatus(const bool yesNo)
//     {
//         injectionStatus_ = yesNo;
//     }

//      bool isInjectionPeriod2() const
//     {
//         return injectionStatus_;
//     }


    bool isInjectionPeriod() const
    {
        return injectionState_;
    }

    bool isOnWall(const SubControlVolumeFace& scvf) const
    {

        return couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf);
    }

    //total substance is written in a file -> shows the hyporheic exchange
    template<class GridVariables, class SolutionVector>
    void calculateGlobalMass(const GridVariables& gridVars, const SolutionVector& sol)
    {
        auto fvGeomentry = localView(this->fvGridGeometry());
        auto elemVolVars = localView(gridVars.curGridVolVars());

        Dune::FieldVector<Scalar, FluidSystem::numComponents> totalMass(0.0);

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            fvGeomentry.bindElement(element);
            elemVolVars.bindElement(element, fvGeomentry, sol);

            for (const auto& scv : scvs(fvGeomentry))
            {
                const auto& volVars = elemVolVars[scv];

                for (int i = 0; i < totalMass.size(); ++i)
                    totalMass[i] += volVars.molarDensity() * volVars.moleFraction(0, i) * scv.volume();
            }
        }


        file_ << "Total mass in free flow is " << totalMass << std::endl;

    }

    // \}

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->fvGridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_; }

    Scalar eps_;
    Scalar inletVelocity_;
    Scalar pressure_;
    Scalar inletMoleFraction_Fe2O3_;
    Scalar inletMoleFraction_Zn_;
    Scalar inletMoleFraction_Cu_;
    Scalar inletMoleFraction_PO4_3_;
    Scalar dissipation_;
    Scalar gamma_;

    // TODO Scalar turbulentKineticEnergy_;

//     bool injectionStatus_;
    bool injectionState_;
    GravityVector gravity_;
    std::ofstream file_;

    std::shared_ptr<CouplingManager> couplingManager_;
};
} //end namespace

#endif // DUMUX_STOKES_SUBPROBLEM_HH
